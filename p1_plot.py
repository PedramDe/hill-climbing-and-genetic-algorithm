import numpy as np
import matplotlib.pyplot as plt

class plot:
	def f(self, x):
		return np.sin(10*np.pi*x)/(2*x) + (x-1)**4

	def df(self, x):
		return -np.sin(10*np.pi*x)/(2*x ** 2)+(5*np.pi*np.cos(10*np.pi*x))/x+4*(x-1) ** 3

	def savePlot(self, x, name):
		r = np.arange(0.5, 2.5, 0.01)
		fig, ax = plt.subplots()
		ax.plot(r, self.f(r))
		plt.plot(x, self.f(x),'ro') 
		plt.xlabel("x"), plt.ylabel("f(x)")
		plt.savefig(name + ".png")
