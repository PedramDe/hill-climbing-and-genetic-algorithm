import matplotlib.pyplot as plt
import random, math
from functools import reduce

class Card_Genetic:
    SUM_GOAL = 36
    PROD_GOAL = 360
    def __init__(self, mutation_rate, generation_limit, population_limit):
        self.mutation_rate = mutation_rate
        self.generation_limit = generation_limit
        self.population_limit = population_limit
        self.genes = [[random.randint(0,1) for i in range(10)] for i in range(population_limit)]
        self.population = []
        
    def utility_sum(self):
        return sum([p[1] for p in self.population])

    def pile1(self, gene):
        return [i+1 for i in range(10) if gene[i] == 0]

    def pile2(self, gene):
        return [i+1 for i in range(10) if gene[i] == 1]

    def utility_func(self, gene):
        sumi = sum(self.pile1(gene))
        prod = reduce(lambda x, y: x*y, self.pile2(gene) + [1])
        sum_error = (sumi - self.SUM_GOAL) / self.SUM_GOAL
        prod_error = (prod - self.PROD_GOAL) / self.PROD_GOAL
        combined_error = math.fabs(sum_error) + math.fabs(prod_error)
        return combined_error

    def got_answer(self):
        m = min([(p[1], p[0]) for p in self.population])
        return m[0] == 0, m[1]

    def cross_over(self):
        k = random.randint(0, 10)
        for i in range(0, len(self.population), 2):
            gene1 = self.population[i][0]
            gene2 = self.population[i+1][0]
            child1 = gene1[:k] + gene2[k:]
            child2 = gene2[:k] + gene1[k:]
            self.population += [(child1, self.utility_func(child1))]
            self.population += [(child2, self.utility_func(child2))]

    def eliminate(self):
        sorted(self.population, key=lambda tup: tup[1], reverse=True)
        genes = [self.population[0] for i in range(self.population_limit)]

    def mutation(self):
        self.genes = [[1-gene[i] if random.random()<self.mutation_rate else gene[i] for i in range(10)] for gene in self.genes]

    def gentic_solver(self):
        generations_utility = []
        for t in range(0, self.generation_limit):
            self.population = [(gene, self.utility_func(gene)) for gene in self.genes] 
            generations_utility += [self.utility_sum()]

            result, ans = self.got_answer()
            if result == True:
                self.show_answer(t, ans)
                break

            self.cross_over()
            self.eliminate()
            self.mutation()
        self.plot_utility(generations_utility)

    def show_answer(self, generation, gene):
        print("Afer " + str(generation) + " generation:" + "\n")
        print("Pile 1(sum): ", self.pile1(gene), '\n')
        print("Pile 2(prod): " , self.pile2(gene), '\n')

    def plot_utility(self, f):
        plt.title("utility of population")
        plt.plot(f)
        plt.xlabel("generation"), plt.ylabel("utility")
        plt.savefig("genetic.png")


def main():
    mutation_rate = 0.4
    generation_limit = 200
    population_limit = 150
    Card_Genetic(mutation_rate, generation_limit, population_limit).gentic_solver()

if __name__ == "__main__":
    main()