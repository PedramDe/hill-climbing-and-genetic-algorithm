from p1_plot import plot
import random

def hill_climbing(f, df, learning_rate=10**-3, precision=10**-5):
	last_step_size = 1
	point = random.uniform(.5, 2.5)
	counter = 0
	
	while last_step_size > precision and counter < 1000:
		delta = df(point) * learning_rate
		point += (-delta)
		last_step_size = abs(delta)
		counter += 1
	return f(point), point

def main():
	f, df = plot().f, plot().df
	min_point = min([hill_climbing(f, df) for i in range(1000)])
	plot().savePlot(min_point[1], "hill_climbing")
	print(min_point)

if __name__ == "__main__":
	main()
	pass