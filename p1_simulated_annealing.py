from p1_plot import plot
import random, math

def simulated_annealing(f, cooling_rate=.999, precision=10**-6):
    temp = 10**4
    search_range =0.5
    point = random.uniform(.5, 2.5)
    
    while temp > precision:
        temp *= cooling_rate
        new_point = random.uniform(point-search_range, point+search_range)
        new_point = .5 if new_point<.5 else new_point
        new_point = 2.5 if new_point>2.5 else new_point
        delta = f(new_point) - f(point)
        if(delta<0):
            point = new_point
        elif random.random() < math.e**(-delta/temp):
            point  = new_point 
    return f(point), point

def main():
    f = plot().f
    min_point = simulated_annealing(f)
    plot().savePlot(min_point[1], "simulated_annealing")
    print(min_point)

if __name__ == "__main__":
    main()